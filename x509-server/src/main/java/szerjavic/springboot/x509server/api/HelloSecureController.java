package szerjavic.springboot.x509server.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("secure")
public class HelloSecureController {


    @GetMapping("/hello")
    public String hello(Principal principal) {
        return String.format("Hello %s from secure controller", principal.getName());
    }

}
