package szerjavic.springboot.x509server.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("insecure")
public class HelloInsecureController {


    @GetMapping("/hello")
    public String hello() {
        return "Hello from insecure controller";
    }

}
