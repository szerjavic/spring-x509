package szerjavic.springboot.x509client.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/client")
public class ClientRestController {

    private final RestTemplate insecureRestTemplate;
    private final RestTemplate secureRestTemplate;


    public ClientRestController(RestTemplate insecureRestTemplate, RestTemplate secureRestTemplate) {
        this.insecureRestTemplate = insecureRestTemplate;
        this.secureRestTemplate = secureRestTemplate;
    }


    @GetMapping("insecure/hello")
    public String getHello() {
        return insecureRestTemplate.getForObject("http://localhost:8080/insecure/hello", String.class);
    }

    @GetMapping("secure/hello")
    public String getSecureHello() {
        return secureRestTemplate.getForObject("https://localhost:8443/secure/hello", String.class);
    }

}
